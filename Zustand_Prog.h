/* 
 * File:   Zustand_Prog.h
 * Author: abe713
 *
 * Created on 4. Dezember 2013, 14:54
 */

#ifndef ZUSTAND_PROG_H
#define	ZUSTAND_PROG_H

extern void zustand();

typedef enum {
    Anfangszustand,
    Betriebszustand,
    Transport,
    Lichtschranke,
    Messung_OK,
    Messung_nicht_OK,
    Weiche,
    Metallisch,
    Nicht_Metallisch,
    Rutsche,
    Auslauf
} Zustand;

Zustand Aktuell_zustand;


#endif	/* ZUSTAND_PROG_H */

