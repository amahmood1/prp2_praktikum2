#include <stdio.h>
#include <stdlib.h>
#include "cbw.h"
#include "modul.h"



//------------------------------------------------------------------------------
//Register am anfang null setzen
//------------------------------------------------------------------------------

static unsigned short sensor_reg = 0, aktor_reg = 0, uebergabe = 0, einsetzen = 0, loeschen = 0;

//------------------------------------------------------------------------------
//                                                         Exportierten Routinen
//------------------------------------------------------------------------------

void config_ports(void) // Initialisierung
{
    cbDConfigPort(0, FIRSTPORTA, DIGITALOUT);
    cbDConfigPort(0, FIRSTPORTB, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCH, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCL, DIGITALOUT);
}

void updateProcessImage(void) {
    unsigned short temp = 0;


    cbDIn(0, FIRSTPORTCH, &sensor_reg);
    sensor_reg = sensor_reg << 8;
    cbDIn(0, FIRSTPORTB, &temp);
    sensor_reg |= temp;
}

void applyOutputToProcess(void) {
    unsigned short temp = 0;


    cbDOut(0, FIRSTPORTA, aktor_reg);
    temp = aktor_reg;
    temp = temp >> 8;
    cbDOut(0, FIRSTPORTCL, temp);
}

int isBitSet(unsigned short uebergabe) {
    unsigned short k;
    unsigned short ergebnis = 0;
    ergebnis = uebergabe & sensor_reg;
    if (ergebnis != 0)
        return unsigned short k=1;
    else
        return unsigned short k=0;
}

void setBitInOutput(unsigned short einsetzen) {

    aktor_reg = aktor_reg | einsetzen;

}

void clearBitInOutput(unsigned short loeschen) {

    aktor_reg &= ~loeschen;

}

void reset_aktoren() {
    aktor_reg = 0;
    applyOutputToProcess();
}



