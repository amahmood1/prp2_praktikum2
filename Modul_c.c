
#include <stdio.h>
#include <stdlib.h>
#include "cbw.h"
#include "Modul_h.h"



//------------------------------------------------------------------------------
//Register am anfang null setzen
//------------------------------------------------------------------------------

static unsigned short sensor_reg = 0, aktor_reg = 0, uebergabe = 0, einsetzen = 0, 
                      loeschen = 0, pos_aenderung=0,neg_aenderung=0;
                      


void config_ports(void) // Initialisierung
{
    cbDConfigPort(0, FIRSTPORTA, DIGITALOUT);
    cbDConfigPort(0, FIRSTPORTB, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCH, DIGITALIN);
    cbDConfigPort(0, FIRSTPORTCL, DIGITALOUT);
}

void updateProcessImage(void) {
    unsigned short temp = 0;
    unsigned short sensor_reg_alt =0;
    unsigned short sensor_reg_neu=0;
    unsigned short sensor_reg_vergleich=0;
    sensor_reg_alt= sensor_reg;


    cbDIn(0, FIRSTPORTCH, &sensor_reg);
    sensor_reg = sensor_reg << 8;
    cbDIn(0, FIRSTPORTB, &temp);
    sensor_reg |= temp;
    
    sensor_reg_neu=sensor_reg;
    sensor_reg_vergleich=sensor_reg_alt^ sensor_reg_neu;

    pos_aenderung= sensor_reg_alt & sensor_reg_vergleich;
    neg_aenderung= sensor_reg_neu & sensor_reg_vergleich;

}

void applyOutputToProcess(void) {
    unsigned short temp = 0;


    cbDOut(0, FIRSTPORTA, aktor_reg);
    temp = aktor_reg;
    temp = temp >> 8;
    cbDOut(0, FIRSTPORTCL, temp);
}

int isBitSet(unsigned short uebergabe) {
    unsigned short k;
    unsigned short ergebnis = 0;
    ergebnis = uebergabe & sensor_reg;
    if (ergebnis != 0)
        return 1;
    else
        return 0;
}

void setBitInOutput(unsigned short einsetzen) {

    aktor_reg = aktor_reg | einsetzen;

}

void clearBitInOutput(unsigned short loeschen) {

    aktor_reg &= ~loeschen;

}


int pos_Flanke (unsigned short uebergabe){

     unsigned short erg =0;   
     erg=pos_aenderung & uebergabe;
        if (erg>0)
            return 1;

            else

            return 0;
};

int neg_Flanke (unsigned short uebergabe){

    unsigned short  erg=0;

    erg= neg_aenderung & uebergabe;
        if (erg>0)
            return 1;

        else
            return 0;

};

void reset_aktoren() {
    aktor_reg = 0;
    applyOutputToProcess();
}