/* 
 * File:   liste.h
 * Author: said
 *
 * Created on 4. Januar 2014, 09:41
 */
#include <time.h>

#ifndef LISTE_H
#define	LISTE_H

#
    typedef struct liste {
    
        time_t begin;
        time_t end;
        int mettalisch;
        int hoehe;
        struct liste * next;
    } liste_t;
    
    typedef liste_t * liste_ptr;

extern void liste_anfuegen (liste_ptr *head,time_t begin,int hoehe,int mettalisch,time_t end);
extern void loeschen(liste_ptr *head);
extern void listen_zeigen(liste_ptr *head);
extern void file_ausgabe(liste_ptr *head);

#endif	/* LISTE_H */

