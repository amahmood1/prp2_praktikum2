

#include "liste.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>



//Aus code snip 3

void liste_anfuegen(liste_ptr *head, time_t anfang, int hoehe, int metall, time_t end) {
    // Neue Liste wird im Heap angelegt und mit Daten gefüllt

    liste_ptr current = (liste_t*) malloc(sizeof (liste_t));
    
    if (current != NULL) {
        current-> begin = anfang;
        current-> hoehe = hoehe;
        current-> mettalisch = metall;
        current-> end = end;
        current-> next = NULL;
        // Wenn keine Liste existiert, wird eine neue Liste am Head platziert

        liste_ptr iterator_ptr = *head;
        //Iterator bekommt den Inhalt von head zugewiesen
        
        if (iterator_ptr == NULL) {
            *head = current;
        } else { 
            // falls eine Liste bereits existiert, wird das Ende der Kette gesucht
            // und eine neue Liste angefügt
            while (iterator_ptr->next != 0) {
                iterator_ptr = iterator_ptr->next;
            }
            iterator_ptr->next = current;
        }
    }
}

void loeschen(liste_ptr *head) { //gesamte Listen-Kette wird gelöscht
    if (*head != NULL) {
        liste_ptr iterator = *head->next;
        liste_ptr vorgaenger = *head;

        while (iterator != NULL) {
            free(vorgaenger);
            vorgaenger = iterator;
            iterator = vorgaenger -> next;
        }
        free(vorgaenger); // Letzte Liste in der Kette muss seperat frei gegeben werden
    }
    
    *head = NULL;
}


void listen_zeigen(liste_ptr *head) { // Messdaten werden auf der Console ausgegeben
    if (*head == 0) {
        printf("Keine Listen zur Ausgabe!");
    } else {
        liste_ptr iterator = *head;     //Definition eines Iterator-Pointers
        while (iterator != 0) {
            printf("Liste: Zeitpunkt Anfang:%d\nMetallisch:%i\nHoehe :%i\n Zeitpunkt Ende:%d\n",
                    iterator->begin, iterator->mettalisch,
                    iterator->hoehe, iterator->end); //
            iterator = iterator->next;
        }

    }
}

void file_ausgabe(liste_ptr *head) { // Messdaten werden in eine externe Datei geschrieben
    liste_ptr iterator = *head;         //Definition eines Iterator-Pointers
    FILE *Werkstueckdaten = 0;
    Werkstueckdaten = fopen("Z:\\Messdaten.csv", "a");
    fprintf(Werkstueckdaten, "Messungsanfang;Metallisch;Hoehe;Messungsende\n");
    while (iterator != 0) {
        fprintf(Werkstueckdaten, "%d;%i;%i;%d\n",
                iterator->begin, iterator->mettalisch,
                iterator->hoehe, iterator->end);
        iterator = iterator->next;
    }
    fclose(Werkstueckdaten);
}


 


