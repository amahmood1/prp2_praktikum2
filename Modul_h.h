/* 
 * File:   modul.h
 * Author: said
 *
 * Created on 22. Oktober 2013, 09:38
 */



#ifndef IO_MODUL_H
#define	IO_MODUL_H

//-(Ausgabe Port)
#define  RECHTSLAUF 0x0001
#define LINKSLAUF 0x0002
#define MOTOR_LANGSAM 0x0004
#define MOTOR_STOP 0x0008
#define WEICHE_AUF 0x0010
#define AMPEL_GRUEN 0x0020
#define AMPEL_GELB 0x0040
#define AMPEL_ROT 0x0080
#define LED_START 0X0100
#define led_reset 0X0200
#define LED_Q1 0X0400
#define LED_Q2 0X0800

// (Eingabe Port)
#define  WS_Einlauf 0x0001
#define WS_Lichtschranke 0x0002
#define Hoehenbereich 0x0004
#define WS_Weiche 0x0008
#define WS_Metall 0x0010
#define Weiche_offen 0x0020
#define Rutsche_voll 0x0040
#define Auslauf_WS 0x0080
#define Taste_Start 0X0100
#define Taste_Stop 0X0200
#define Taste_Reset 0X0400
#define E_Stop 0X0800

extern  unsigned char Anfangszustand;
extern  unsigned char Betriebszustand;
extern  unsigned char Transport;
extern  unsigned char Lichtschranke;
extern  unsigned char Messung_OK;
extern  unsigned char Messung_nicht_OK;
extern  unsigned char Weiche;
extern  unsigned char Metallisch;
extern  unsigned char Nicht_Metallisch;
extern  unsigned char Rutsche;
extern  unsigned char Auslauf;
extern  unsigned char Aktuell_zustand;

extern void config_ports(void);
extern void updateProcessImage(void);
extern void applyOutputToProcess(void);
extern int isBitSet(unsigned short uebergabe);
extern void setBitInOutput(unsigned short einsetzen);
extern void clearBitInOutput(unsigned short loeschen);
extern void reset_aktoren(void);
extern void zustand(char);

#endif	/* MODUL1_H */