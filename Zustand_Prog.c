#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Modul_h.h"
#include "cbw.h"
#include "liste.h"

typedef enum {
    true = 1, false = 0
} T_Logical;

void zustand(char Aktuell_zustand) {
    
    int metallisch = 0;
    int hoehe = 0;
    time_t begin;
    time_t end;
    liste_ptr head = NULL;
    
    config_ports();
    reset_aktoren();

    Aktuell_zustand = Anfangszustand;
    while (true) {
        int x;
        switch (Aktuell_zustand) {

            case 'Anfangszustand':
                reset_aktoren();
                setBitInOutput(LED_START);


                if (pos_Flanke(Taste_Start)) {
                    Aktuell_zustand = Betriebszustand;
                    loeschen(&head);
                };

                break;


            case 'Betriebszustand':
                clearBitInOutput(LED_START);
                setBitInOutput(AMPEL_GRUEN);

                if (pos_Flanke(Taste_Start)) {
                    Aktuell_zustand = Anfangszustand;
                    listen_zeigen(&head);
                    file_ausgabe(&head);
                };

                if (neg_Flanke(WS_Einlauf)) {
                    Aktuell_zustand = Transport;
                    time(&begin);
                };
                break;


            case 'Transport':
                setBitInOutput(RECHTSLAUF);
                setBitInOutput(AMPEL_GELB);
                clearBitInOutput(AMPEL_GRUEN);


                if (neg_Flanke(WS_Lichtschranke)) {
                    Aktuell_zustand = Lichtschranke;
                };

                break;

            case 'Lichtschranke':
                setBitInOutput(AMPEL_ROT);
                clearBitInOutput(RECHTSLAUF);
                clearBitInOutput(AMPEL_GELB);

                if (isBitSet(Hoehenbereich) == 1) {
                    Aktuell_zustand = Messung_OK;
                    x = isBitSet(Hoehenbereich);
                    hoehe=1;
                } else {
                    Aktuell_zustand = Messung_nicht_OK;
                    x = isBitSet(Hoehenbereich);
                    hoehe=0;
                };
                break;

            case 'Messung_OK':
                setBitInOutput(AMPEL_GRUEN);
                setBitInOutput(RECHTSLAUF);
                setBitInOutput(MOTOR_LANGSAM);
                clearBitInOutput(AMPEL_ROT);
                setBitInOutput(LED_Q1);

                printf(" \nWekstueck im Hoehenbereich? \nBitwert: %i\n\n", x);

                if (isBitSet(WS_Weiche)) {
                    Aktuell_zustand = Weiche;
                };
                break;

            case 'Messung_nicht_OK':
                setBitInOutput(AMPEL_ROT);
                setBitInOutput(RECHTSLAUF);
                setBitInOutput(MOTOR_LANGSAM);
                printf(" \nWekstueck im Hoehenbereich? \nBitwert: %i\n\n", x);

                if (isBitSet(WS_Weiche)) {
                    Aktuell_zustand = Weiche;
                };
                break;

            case 'Weiche':
                clearBitInOutput(RECHTSLAUF);
                clearBitInOutput(MOTOR_LANGSAM);
                clearBitInOutput(AMPEL_GRUEN);
                setBitInOutput(AMPEL_ROT);

                if (isBitSet(WS_Metall) == 1) {
                    Aktuell_zustand = Metallisch;
                    metallisch=1;
                };

                if (isBitSet(WS_Metall) == 0) {
                    Aktuell_zustand = Nicht_Metallisch;
                    metallisch=0;
                };
                break;

            case 'Metallisch':
                setBitInOutput(AMPEL_GELB);
                setBitInOutput(AMPEL_ROT);
                setBitInOutput(RECHTSLAUF);

                if (isBitSet(Rutsche_voll) == 0) {
                    Aktuell_zustand = Rutsche;
                    liste_anfuegen ( &head, begin, hoehe, metallisch, end);
                    time (&end);
                };
                break;

            case 'Rutsche':
                clearBitInOutput(RECHTSLAUF);
                clearBitInOutput(AMPEL_GELB);
                setBitInOutput(AMPEL_ROT);

                if (isBitSet(Rutsche_voll) == 1 & isBitSet(WS_Einlauf)) {
                    Aktuell_zustand = Transport;
                };
                break;

            case 'Nicht_Metallisch':
                clearBitInOutput(AMPEL_ROT);
                setBitInOutput(AMPEL_GELB);
                setBitInOutput(RECHTSLAUF);
                setBitInOutput(WEICHE_AUF);
                setBitInOutput(LED_Q2);

                if (isBitSet(Auslauf_WS) == 0) {
                    Aktuell_zustand = Auslauf;
                    liste_anfuegen ( &head, begin, hoehe, metallisch, end);
                    time (&end);
                };
                break;

            case 'Auslauf':
                clearBitInOutput(RECHTSLAUF);
                clearBitInOutput(AMPEL_ROT);
                setBitInOutput(AMPEL_GRUEN);
                clearBitInOutput(WEICHE_AUF);
                clearBitInOutput(LED_Q2);

                if (isBitSet(Auslauf_WS) == 1 & isBitSet(WS_Einlauf)) {
                    Aktuell_zustand = Transport;
                };
                break;

        }
        applyOutputToProcess();
    }
}